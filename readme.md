# Car Rental Booking Engine #

This Wordpress plugin adds a car rental search box to your blog so visitors can check availability and compare prices of over 750 car hire companies worldwide.T

The plugin is developed by [Hispacar](http://www.hispacar.com). It will automatically calculate prices and availability in more than 30.000 destinations around the world. It is a great source of income for bloggers as every booking automatically generates a commission.

**Main Features**

+ show car hire prices in more than 30.000 destinations worldwide
+ check availability in more than 150 different countries
+ list vehicles from over 750 suppliers worldwide and growing
+ earn commissions for every booking made on your website

Install the plugin now and start earning money with Hispacar today.

## Installation ##
This is the latest stable build of the car hire price comparison plugin from [Hispacar](http://www.hispacar.com/car-hire/)

To install the Car Rental Booking Engine for WordPress, we recommend you download the plugin directly from the [WordPress plugin repository](https://wordpress.org/plugins/car-rental-booking-engine-by-hispacar/). If you would like to contribute to or test the developer build please contact our development team.

## Support ##
For support, we recommend to use the wordpress plugin repository. You can also contact us directly at the [plugin's official website](http://www.hispacar.com/wordpress-plugins/search/en/)

## Usage ##
The plugin can be used as a car hire search box in any page or post on your blog or as a search widget in your sidebar. Visit the examples below if you want to see the Hispacar search boxes in action live.

* [Discover Mijas](http://www.discovermijas.com/). A great example of the car rental search box used as a widget in the sidebar.
* [Trees for Mijas](http://www.treesformijas.org/en/blog/proud-sponsor). See the plugin in action on a post. The plugin can also be used on a page.

## Bugs ##
The plugin has been tested up to WP version 4. If you find an issue, let us know!

## Frequently Asked Questions ##

**How can I get an Affiliate ID?**
Visit the plugin's [official website](http://www.hispacar.com/wordpress-plugins/search/en/) to register or use the corresponding link in the setup screen of the plugin You can also [register here](http://www.hispacar.com/wordpress-plugins/search/en/register/) and request an affiliate ID on the official website of the plugin.

**Do I need to register in order to use the plugin?**
You can use the plugin without registration, but if you want to monetize the plugin you need to request an affiliate ID.

## Plugin's official Website ##
More information about the plugin and a detailled step-by-step installation and usage guide can be found on the plugin's official website:

* [Features](http://www.hispacar.com/wordpress-plugins/search/en/#Features)
* [Installation](http://www.hispacar.com/wordpress-plugins/search/en/#Installation)
* [Setup](http://www.hispacar.com/wordpress-plugins/search/en/#Setup)
* [Usage](http://www.hispacar.com/wordpress-plugins/search/en/#Usage)
* [Affiliates](http://www.hispacar.com/wordpress-plugins/search/en/#Affiliates)
* [Revenue](href="http://www.hispacar.com/wordpress-plugins/search/en/#Revenue)
* [Demo](http://www.hispacar.com/wordpress-plugins/search/en/#Demo)
* [Screenshots](http://www.hispacar.com/wordpress-plugins/search/en/#Screenshots)
* [Support](http://www.hispacar.com/wordpress-plugins/search/en/#Support)
* [Feedback](http://www.hispacar.com/wordpress-plugins/search/en/#Feedback)